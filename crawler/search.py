from pprint import pprint

from elasticsearch import Elasticsearch


def main5():
    es = Elasticsearch()
    conf = {
        'w': [5, 4, 3],
        'cluster': None,
        'rank': False,
    }

    while True:
        print('')
        print(' (1) Query')
        print(' (2) See Document')
        print(' (3) Set Weights')
        print(' (4) Select Cluster')
        print(' (5) Toggle PageRank')
        choice = int(input('  ?  '))

        if choice == 1:
            query = input('Query? ')
            w = conf['w']
            es_query = {
                'function_score': {
                    'query': {
                        'bool': {
                            'must': {
                                'multi_match': {
                                    'query': query,
                                    'type': 'most_fields',
                                    'fields': ['title^{}'.format(w[0]),
                                               'intro^{}'.format(w[1]),
                                               'text^{}'.format(w[2])],
                                },
                            },
                        },
                    },
                    'functions': [
                        {
                            'script_score': {
                                'script': {
                                    'lang': 'painless',
                                    'inline': '_score',
                                },
                            }
                        }
                    ]
                },
            }
            if conf['cluster']:
                es_query['function_score']['query']['bool']['filter'] = {
                    'term': {
                        'cluster': conf['cluster'],
                    }
                }
            if conf['rank']:
                ranked_score = "_score * (0.4 + doc['rank'].value * 10)"
                es_query['function_score']['functions'][0]['script_score']['script']['inline'] = ranked_score
            results = es.search('wiki', 'fa', {'query': es_query})
            results = results['hits']['hits']
            for result in results:
                print('[{:.2f}]\t[{:.2f}]\t{}'.format(result['_score'], result['_source']['rank'], result['_source']['id']))
        elif choice == 2:
            doc_id = input('Document Id? ')
            doc = es.get('wiki', doc_id)['_source']
            doc['text'] = 'https://fa.wikipedia.org/wiki/{}'.format(doc_id)
            doc['links'] = ','.join(doc['links'][:20])
            pprint(doc)
        elif choice == 3:
            print('Weights are: title={}, intro={}, text={}'.format(*conf['w']))
            w_title = int(input('W_title? '))
            w_intro = int(input('W_intro? '))
            w_text = int(input('W_text? '))
            conf['w'] = [w_title, w_intro, w_text]
            print('Weights updated!')
        elif choice == 4:
            print('Currently searching in cluster: {}'.format(conf['cluster']))
            print('Available Clusters are: ...')
            cluster = int(input('Cluster Number? '))
            conf['cluster'] = cluster or None
            print('Cluster updated!')
        elif choice == 5:
            print('PageRank in currently {}'.format('enabled' if conf['rank'] else 'disabled'))
            page_rank_enable = int(input('Enable? [0/1] '))
            conf['rank'] = bool(page_rank_enable)
            print('PageRank updated!')
        elif choice == 0:
            break


if __name__ == '__main__':
    main5()
