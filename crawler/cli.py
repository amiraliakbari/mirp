import subprocess
import sys

from elasticsearch import ConnectionError as ElasticConnectionError

from load import main2
from cluster import main3
from rank import main4
from search import main5


def main():
    while True:
        choice = int(input('==> Part? '))
        if choice == 1:
            subprocess.call(['scrapy', 'crawl', 'wiki'])
        elif choice == 2:
            main2()
        elif choice == 3:
            main3()
        elif choice == 4:
            main4()
        elif choice == 5:
            main5()
        elif choice == 0:
            break


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nbye')
    except ElasticConnectionError:
        print('Can not connect to ElasticSearch')
        sys.exit(1)
