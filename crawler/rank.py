import networkx as nx
from tqdm import tqdm
from elasticsearch import Elasticsearch


def page_rank(graph, alpha=0.85):
    ranks = {}
    V = len(graph)

    for key, node in graph.nodes(data=True):
        ranks[key] = 1 / float(V)

    for _ in tqdm(range(15)):
        for key, node in graph.nodes(data=True):
            rank_sum = 0
            neighbors = graph.out_edges(key)
            for n in neighbors:
                outlinks = len(graph.out_edges(n[1]))
                if outlinks > 0:
                    rank_sum += (1 / float(outlinks)) * ranks[n[1]]
            ranks[key] = ((1 - float(alpha)) * (1 / float(V))) + alpha * rank_sum

    return ranks


def load_pages(es):
    pages = es.search('wiki', 'fa', {}, params={'_source_exclude': ['text', 'info'], 'size': 100})
    pages = pages['hits']['hits']
    ids = set(p['_id'] for p in pages)
    graph = nx.DiGraph()
    graph.add_nodes_from(ids)
    for p in pages:
        page_id = p['_id']
        for l in p['_source']['links']:
            if l in ids and l != page_id:
                graph.add_edge(page_id, l)
    return graph


def save_ranks(es, ranks):
    for page_id, rank in tqdm(ranks.items()):
        es.update('wiki', 'fa', page_id, {'doc': {'rank': rank}})


def main4():
    alpha = float(input('alpha? ') or 0.85)
    es = Elasticsearch()
    print('Loading page data')
    graph = load_pages(es)
    print('n:', len(graph.nodes()), '\t', 'e:', len(graph.edges()))
    print('Calculating page rank')
    ranks = page_rank(graph, alpha=alpha)
    # for k in sorted(([r[1], r[0]] for r in ranks.items()), reverse=True):
    #     print(k[0], k[1])
    print('Saving ranks...')
    save_ranks(es, ranks)
    print('DONE')


if __name__ == '__main__':
    main4()
