import base64
import json
import logging
import os
import random
import re
from urllib.parse import unquote

from bs4 import BeautifulSoup
from scrapy import Spider, Request
from scrapy.exceptions import CloseSpider


class PageGraph:
    def __init__(self):
        try:
            from igraph import Graph
        except ImportError:
            self.graph = None
        else:
            self.graph = Graph()
        self.page_id_map = []
        self.trans = {
            'سعدی': 'Saadi',
        }

    def get_page_id(self, page_id):
        if self.graph is None:
            return
        try:
            return self.page_id_map.index(page_id)
        except ValueError:
            self.page_id_map.append(page_id)
            self.graph.add_vertex(name=page_id)
            return len(self.page_id_map) - 1

    def add_link(self, from_page, to_page):
        if self.graph is None:
            return
        v0 = self.get_page_id(from_page)
        v1 = self.get_page_id(to_page)
        self.graph.add_edge(v0, v1)

    def save_graph(self, filename):
        if self.graph is None:
            return

        from igraph import plot

        names = [self.trans.get(s, s[:7]).replace('_', '‌') for s in self.graph.vs['name']]
        plot(
            self.graph,
            target=filename,
            # layout=self.graph.layout('kk'),
            vertex_label=names,
            vertex_label_size=8,
            vertex_label_angle=0.5,
            vertex_size=25,
            bbox=[3000, 3000],
        )


class WikiSpider(Spider):
    name = 'wiki'
    url_prefix = 'https://fa.wikipedia.org/wiki/'
    en_url_prefix = 'https://en.wikipedia.org/wiki/'
    max_out_degree = 10
    pages_to_crawl = 25

    def __init__(self, *args, **kwargs):
        super(Spider, self).__init__(*args, **kwargs)
        self.crawled_pages = set()
        self.ignored_pages = set()
        self.graph = PageGraph()

    def start_requests(self):
        print('Crawling...     |{}|\r'.format(' ' * self.pages_to_crawl), end='', flush=True)

        urls = [
            'https://fa.wikipedia.org/wiki/%D8%B3%D8%B9%D8%AF%DB%8C',
            'https://fa.wikipedia.org/wiki/%D9%86%D8%A7%D8%AE%D8%AF%D8%A7_%D8%AE%D9%88%D8%B1%D8%B4%DB%8C%D8%AF',
        ]
        for url in urls:
            yield Request(url=url, callback=self.parse)

    @classmethod
    def save_json(cls, page):
        page_id_b64 = base64.b64encode(page['id'].encode('utf8')).decode('ascii')
        page_id_b64 = page_id_b64.replace('/', '_').replace('+', '-')
        page_id_b64 += '.json'
        with open(os.path.join('results', page_id_b64), 'w', encoding='utf8') as f:
            json.dump(page, f, ensure_ascii=False)
            f.write('\n')

    @classmethod
    def strip_tags(cls, html):
        if not html:
            return ''
        return re.sub('<[^<]+?>', '', html)

    @classmethod
    def clean_wiki_text(cls, html):
        if not html:
            return ''

        bs = BeautifulSoup(html, 'lxml')

        def remove_elements(selector):
            for el in bs.select(selector):
                el.extract()

        remove_elements('.mw-editsection')          # edit links
        remove_elements('.mbox-small.plainlinks')   # boxes linking to other wikimedia sites
        remove_elements('.reflist')                 # references
        remove_elements('#toc')                     # table of content
        remove_elements('#siteSub')                 # wikipedia branding
        remove_elements('.dablink')                 # disambiguation links
        remove_elements('script')                   # special tags
        remove_elements('sup.reference')            # reference citation number

        return bs.get_text(' ', strip=True)

    @classmethod
    def parse_info_box(cls, html):
        if not html:
            return {}

        bs = BeautifulSoup(html, 'lxml')

        # remove titles like لقب:
        # for el in bs.select('td > b, br + b'):
        #     el.extract()

        info = {}
        for el in bs.select('th + td, td + td'):
            try:
                v = el.get_text(' ', strip=True)
                kel1 = el.find_previous_sibling('th')
                kel2 = el.find_previous_sibling('td')
                if kel1:
                    k = kel1.get_text(' ', strip=True)
                elif kel2:
                    k = kel2.get_text(' ', strip=True)
                else:
                    continue
                info[k] = v
            except:
                continue

        return info

    @classmethod
    def get_page_id(cls, url):
        url = unquote(url)
        if not url.startswith(cls.url_prefix):
            return None
        return url[len(cls.url_prefix):]

    def parse(self, response):
        page_id = self.get_page_id(response.url)
        if not page_id:
            logging.warning('[WARNING] Invalid page url:', response.url)
            return

        # Skip Wikipedia redirects
        if page_id in self.ignored_pages:
            return
        if response.css('#contentSub .mw-redirectedfrom a.mw-redirect').extract_first():
            self.ignored_pages.add(page_id)
            return

        # Mark as crawled
        if len(self.crawled_pages) >= self.pages_to_crawl:
            raise CloseSpider()
        print('Crawling...     |{}>\r'.format('*' * len(self.crawled_pages)), end='', flush=True)
        self.crawled_pages.add(page_id)

        # Find english name
        en_title = ''
        en_title_el = response.css('.interlanguage-link-target[hreflang=en]::attr(href)').extract_first()
        if en_title_el:
            if en_title_el.startswith(self.en_url_prefix):
                en_title = en_title_el[len(self.en_url_prefix):]
                self.graph.trans[page_id] = en_title

        # Extract
        page = {
            'id': page_id,
            'title': self.strip_tags(response.css('#firstHeading').extract_first()),
            'intro': self.strip_tags(response.css('#mw-content-text > p').extract_first()),
            'text': self.clean_wiki_text(response.css('#mw-content-text').extract_first()),
            'en_title': en_title,
            'info': self.parse_info_box(response.css('.infobox').extract_first()),
        }

        # Find out links
        links = response.css('#mw-content-text a::attr(href)').extract()
        link_urls = set()
        good_fa_links = set()
        page['links'] = set()
        for link in links:
            link = response.urljoin(link)
            link_urls.add(link)

            # Exclude links to outside of Wikipedia
            if not link.startswith(self.url_prefix):
                continue
            link_id = unquote(link[len(self.url_prefix):])

            # Remove #... in the end of link
            hash_ind = link_id.find('#')
            if hash_ind >= 0:
                link_id = link_id[:hash_ind]

            # Save in out links
            page['links'].add(link_id)

            # Exclude already crawled links
            if link_id in self.crawled_pages:
                continue

            # Exclude not interesting links
            if ':' in link_id:
                continue
            has_digit = False
            for digit in '۰۱۲۳۴۵۶۷۸۹':
                if digit in link_id:
                    has_digit = True
                    break
            if has_digit:
                continue

            good_fa_links.add(link_id)

        # Select some links to follow
        if len(good_fa_links) > self.max_out_degree:
            good_fa_links = random.sample(good_fa_links, self.max_out_degree)
        for fa_link in good_fa_links:
            # TODO: also add unsampled vertices
            self.graph.add_link(page_id, fa_link)
        for link in good_fa_links:
            yield Request(self.url_prefix + link, callback=self.parse)

        # Save
        page['links'] = sorted(page['links'])
        self.save_json(page)
        print('Crawling...     |{}\r'.format('*' * len(self.crawled_pages)), end='', flush=True)

    def close(self, reason):
        print('Crawling...     |{}'.format('*' * len(self.crawled_pages)), end='', flush=True)
        print('|        [DONE]')

        print('Drawing Graph...', end='', flush=True)
        # noinspection PyBroadException
        try:
            self.graph.save_graph('page_graph.png')
        except:
            pass
        print('        [DONE]', flush=True)
