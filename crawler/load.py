import glob
import json
import os
import pprint
import sys

from elasticsearch import Elasticsearch, RequestError, ConnectionError as ElasticConnectionError, NotFoundError


def main2():
    while True:
        print(' (1) Create index')
        print(' (2) Load json files in ElasticSearch')
        print(' (3) Check index creation')
        print(' (4) Delete all data')
        choice = int(input('  ?  '))
        print()

        es = Elasticsearch()

        if choice == 1:
            try:
                es.indices.create(index='wiki')
            except RequestError:
                print('Index already exists.')
        elif choice == 2:
            for page_filename in glob.glob(os.path.join('results', '*.json')):
                with open(page_filename, 'r', encoding='utf8') as f:
                    page = json.load(f)
                    page_id = page['id']
                    es.index('wiki', 'fa', page, id=page_id)
        elif choice == 3:
            try:
                stats = es.indices.stats(index='wiki')['indices']['wiki']['primaries']['docs']
            except (NotFoundError, KeyError):
                print('Index not created.')
            else:
                print('from: http://localhost:9200/wiki/_stats')
                pprint.pprint(stats)
        elif choice == 4:
            try:
                es.indices.delete(index='wiki')
            except NotFoundError:
                print('Index already deleted.')
        elif choice == 0:
            break
        else:
            print('Invalid choice')


if __name__ == '__main__':
    try:
        main2()
    except ElasticConnectionError:
        print('Can not connect to ElasticSearch')
        sys.exit(1)
