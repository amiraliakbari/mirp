import collections
import random
import re
import operator
from functools import reduce

from tqdm import tqdm
from elasticsearch import Elasticsearch


L = 5
MAX_KMEAN_LOOPS = 15
STOP_WORDS_FA = set()
WORD_RE = re.compile('\w+')


def word_tokenize(text):
    return WORD_RE.findall(text)


def get_distance(v1, v2):
    """
    :param collections.Counter v1:
    :param collections.Counter v2:
    :return:
    """
    v1.keys()
    keys = v1.keys() | v2.keys()
    return sum((v1.get(k, 0) - v2.get(k, 0)) ** 2 for k in keys) ** 0.5


def centroid(vs):
    n = len(vs)
    s = reduce(operator.add, vs)
    return collections.Counter({k: (v // n) for k, v in s.items()})


class Cluster:
    def __init__(self, points):
        assert len(points) > 0
        self.points = []
        self.centroid = None
        self.update(points)

    def update(self, points):
        old_centroid = self.centroid or collections.Counter()
        self.points = [p for p in points]
        self.centroid = centroid(points)
        return get_distance(old_centroid, self.centroid)

    def err(self):
        return sum(get_distance(point, self.centroid) for point in self.points)

    def get_rep(self):
        s = reduce(operator.add, self.points)
        return [word for word, _ in s.most_common(5)]


def load_pages(es):
    pages = es.search('wiki', 'fa', {}, params={'_source_exclude': ['links'], 'size': 100})
    pages = pages['hits']['hits']
    pages_vectors = []
    for page in pages:
        text = page['_source']['text']
        text_words = (w for w in word_tokenize(text) if w not in STOP_WORDS_FA)
        # noinspection PyArgumentList
        vector = collections.Counter(text_words)
        vector.name = page['_id']
        pages_vectors.append(vector)
    return pages_vectors


def k_means(points, k, cutoff=0):
    initial = random.sample(points, k)
    clusters = [Cluster([p]) for p in initial]

    loop_counter = 0
    while True:
        loop_counter += 1

        cluster_count = len(clusters)
        lists = [[] for _ in range(cluster_count)]

        # assign points to clusters
        for p in points:
            smallest_distance = get_distance(p, clusters[0].centroid)
            cluster_index = 0
            for i in range(cluster_count - 1):
                distance = get_distance(p, clusters[i + 1].centroid)
                if distance < smallest_distance:
                    smallest_distance = distance
                    cluster_index = i + 1
            lists[cluster_index].append(p)
        # print([len(ll) for ll in lists])

        # Should we stop this k-means run?
        biggest_shift = 0.0
        for i in range(cluster_count):
            shift = clusters[i].update(lists[i])
            biggest_shift = max(biggest_shift, shift)
        if biggest_shift < cutoff or loop_counter >= MAX_KMEAN_LOOPS:
            break

    err = sum(cluster.err() for cluster in clusters)
    return clusters, err


def do_clustering(pages):
    prev_err = None
    clusters = []
    for k in tqdm(range(3, L + 1)):
        clusters, err = k_means(pages, k)
        print('Error(k={}) = {:.2f}'.format(k, err))
        if prev_err:
            delta = abs(prev_err - err) / prev_err
            if delta < 0.1:
                print('Selected k={}'.format(k))
                break
        prev_err = err

    return clusters


def save_clusters(es, clusters):
    for i, cluster in tqdm(enumerate(clusters)):
        cluster_id = i + 1
        es.index('cluster', 'k', {
            'words': cluster.get_rep(),
            'pages': [point.name for point in cluster.points],
        }, id=cluster_id)
        for point in cluster.points:
            es.update('wiki', 'fa', point.name, {'doc': {'cluster': cluster_id}})


def load_stop_words():
    with open('stop_words.txt', 'r') as f:
        for l in f:
            w = l.strip()
            STOP_WORDS_FA.add(w)


def main3():
    while True:
        print(' (1) Cluster')
        choice = int(input('  ? '))
        if choice == 1:
            global L
            L = int(input('L? ')) or 25
            es = Elasticsearch()
            print('Loading needed initial data...')
            load_stop_words()
            print('Loading pages...')
            pages = load_pages(es)
            print('Clustering...')
            clusters = do_clustering(pages)
            print('Saving results...')
            save_clusters(es, clusters)
            print('Done')
            print('\n\n\n')
        elif choice == 0:
            break


if __name__ == '__main__':
    main3()
